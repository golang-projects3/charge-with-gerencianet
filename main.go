package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gerencianet/gn-api-sdk-go/gerencianet"
)

func main() {

	creds := map[string]interface{}{
		"client_id":     os.Getenv("CLIENT_GN"),
		"client_secret": os.Getenv("CLIENT_SECRET"),
		"sandbox":       true,
		"timeout":       10,
	}
	
	// Criar um cliente da Gerencianet
	gn := gerencianet.NewGerencianet(creds)
	
	// Definir os dados da cobrança
	items := []map[string]interface{}{
		{
				"name":         "Produto 1",
				"value":        1000,
				"amount":       2,
		},
	}

	shippings := []map[string]interface{} {
		{
			"name": "Default Shipping Cost",
			"value": 100,
		},
	}

	body := map[string]interface{}{
		"items":   items,
		"shippings": shippings,
	}


	
	// Criar a cobrança
	charge, err := gn.CreateCharge(body)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(charge)
	
}